import pytest
from mock import patch

import pygame as pg

from world_map import MapInfo


def sort_vectors(vectors):
    return sorted(vectors, key=lambda v: v[0] + v[1] / 10)


@pytest.fixture
def map_info():
    map_array = list(
        [
            [1, 0, 1],
            [1, 1, 0],
            [1, 0, 0],
        ]
    )
    with patch.object(MapInfo, "load_map_array", return_value=map_array):
        return MapInfo("watever")


def test_get_list_of_available_cords(map_info):
    assert map_info.get_available_tiles() == sorted([(0, 0), (2, 0), (0, 1), (1, 1), (0, 2)])


@pytest.mark.parametrize(
    "input_pos, expected",
    (((0, 1), True), ((1, 0), False)),
)
def test_is_tile_available(map_info, input_pos, expected):
    assert map_info.is_tile_available(input_pos) is expected


@pytest.mark.parametrize(
    "input_origin, expected",
    (
        ((0, 2), [(0, 1)]),
        ((2, 0), []),
        ((0, 1), [(0, 0), (0, 2), (1, 1)]),
    ),
)
def test_get_list_of_nearest_available_tiles(map_info, input_origin, expected):
    result = map_info.get_nearest_available_tiles(pg.Vector2(input_origin))
    assert sort_vectors(result) == sort_vectors([pg.Vector2(v) for v in expected])


@pytest.fixture
def big_map_info():
    map_array = list(
        [
            [1, 0, 1, 1, 1],
            [1, 1, 1, 1, 1],
            [1, 0, 0, 1, 0],
        ]
    )
    with patch.object(MapInfo, "load_map_array", return_value=map_array):
        return MapInfo("watever")


@pytest.mark.parametrize(
    "input_origin, input_range, expected",
    (
        ((0, 0), 1, [(0, 1)]),
        ((0, 0), 2, [(0, 0), (0, 1), (0, 2), (1, 1)]),
        ((0, 0), 3, [(0, 0), (0, 1), (0, 2), (1, 1), (2, 1)]),
        ((0, 0), 4, [(0, 0), (0, 1), (0, 2), (1, 1), (2, 1), (2, 0), (3, 1)]),
    ),
)
def test_get_list_of_available_tiles_in_range(big_map_info, input_origin, input_range, expected):
    result = big_map_info.get_available_tiles_in_range(input_origin, input_range)
    assert result == set(expected)
