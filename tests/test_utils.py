import pytest
import pygame as pg

from utils import get_distance_taxi, get_directions_to_target


@pytest.mark.parametrize(
    "p1, p2, expected",
    (
        ((0, 0), (0, 1), 1),
        ((0, 0), (2, 1), 3),
        ((1, 2), (5, 5), 7),
    ),
)
def test_get_distance_taxi(p1, p2, expected):
    assert get_distance_taxi(p1, p2) == expected


@pytest.mark.parametrize(
    "source, target, expected",
    (
        ((0, 0), (0, 1), ((0, 1),)),
        (
            (0, 0),
            (2, 1),
            (
                (1, 0),
                (0, 1),
            ),
        ),
        (
            (5, 5),
            (4, 2),
            (
                (-1, 0),
                (0, -1),
            ),
        ),
    ),
)
def test_get_directions_to_target(source, target, expected):
    assert get_directions_to_target(source, target) == list(map(pg.Vector2, expected))
