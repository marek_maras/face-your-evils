from random import choice
from typing import Optional

import pygame

from players_bar import PlayersBar
from utils import (
    load_sound,
    load_image,
    Weapon,
    GameSignalsQueue,
    GameSignal,
    get_distance_taxi,
    get_directions_to_target,
)
from world_map import WorldMap


class Character:
    action_points = 3
    movement_cost = 3

    def __init__(
        self,
        name,
        hp,
        dmg,
        screen_location: pygame.Rect,
        portrait_file_path,
        draw_settings: dict,
        sound_movement: pygame.mixer.Sound = None,
        sound_attack=None,
    ):
        # char properties
        self.name = name
        self.hp = hp
        self.dmg = dmg
        self.action_points = self.__class__.action_points

        # map properties
        self.position: Optional[pygame.Vector2] = None
        self.map_portrait: Optional[pygame.Surface] = None

        # states
        self.action_ongoing = False
        self.active = False
        self.turn_finished = False
        self.alive = True

        # events
        self.mouse_over = False  # mouse over character surface
        self.hp_lost = 0

        # animation
        self.movement_pixels_per_frame = 5
        self.previous_tile_pos = None
        self.render_map_position_pixels = None

        # sounds
        self.sounds_channel: pygame.mixer.Channel = None
        self.sound_movement = sound_movement if sound_movement else load_sound("tupanie0.mp3")
        self.sound_attack = sound_attack if sound_attack else load_sound("chtrz0.mp3")

        # gui settings
        self.screen_rect = screen_location
        self.font = pygame.font.Font(None, draw_settings["font_size"])
        self.font_color = draw_settings["font_color"]
        self.surface = pygame.Surface(self.screen_rect.size).convert_alpha()
        self.background_color = draw_settings["background_color"]
        self.mouse_over_color = draw_settings["mouse_over_color"]
        self.border_size = draw_settings["character"]["border_size"]
        self.border_blink_alpha = 0
        self.border_blink_step = 4
        self.hp_flash_alpha = 255
        self.hp_flash_alpha_step = 4
        self.portrait, self.portrait_dest_rect = self.setup_portrait(portrait_file_path, draw_settings["portrait_size"])

    def handle_signals(self, signal_queue):
        pass

    def init_for_turn(self, players_bar: PlayersBar = None):
        self.active = True
        self.action_points = self.__class__.action_points

    def is_ready_for_map(self):
        return bool(self.position and self.map_portrait)

    def init_for_local_map(self, local_map: WorldMap, start_pos):
        self.position = start_pos
        self.previous_tile_pos = self.position
        self.render_map_position_pixels = local_map.tile_cords_to_map_pixels(self.position)
        self.map_portrait = local_map.fit_surface_to_tile(self.portrait)

    def move(self, vector: pygame.Vector2):
        if vector == pygame.Vector2(0, 0):
            return
        self.previous_tile_pos = self.position.copy()
        self.position += vector
        # todo: flip image if direction changed
        self.action_ongoing = True
        self.action_points -= 3
        self.sounds_channel = self.sound_movement.play()

    def attack(self, target: "Character"):
        target.reduce_health(self.dmg)
        self.sound_attack.play()
        self.action_points -= 3

    def end_turn(self):
        self.turn_finished = True
        self.active = False

    def reduce_health(self, amount):
        self.hp -= amount
        if self.hp <= 0:
            self.die()
        # gui related
        self.hp_lost = amount

    def die(self):
        self.hp = 0
        self.alive = False

    def update(self, local_map: WorldMap):
        if self.action_ongoing:
            movement_vector = self.position - self.previous_tile_pos
            if movement_vector:
                if not self.sounds_channel.get_busy():
                    self.sounds_channel = self.sound_movement.play()
                render_map_destination_pixels = local_map.tile_cords_to_map_pixels(self.position)
                for i in range(self.movement_pixels_per_frame):
                    self.render_map_position_pixels += movement_vector
                    if self.render_map_position_pixels == render_map_destination_pixels:
                        self.previous_tile_pos = self.position.copy()
                        self.action_ongoing = False

    def draw_on_combat_map(self, combat_map: WorldMap):
        dirty_map_portrait = self.map_portrait.copy()
        if self.active:
            self.draw_active_border_combat(dirty_map_portrait)
        combat_map.draw(self.render_map_position_pixels, dirty_map_portrait)

        map_portrait_pixel_rect = self.map_portrait.get_rect().move(self.render_map_position_pixels)
        self._draw_name(
            combat_map.map_surface,
            (map_portrait_pixel_rect.centerx, map_portrait_pixel_rect.bottom),
        )
        self._draw_hp(
            combat_map.map_surface,
            (map_portrait_pixel_rect.centerx, map_portrait_pixel_rect.top),
        )

    def draw_active_border_combat(self, surface):
        border_alpha = surface.copy()
        green_color = pygame.Color("green")

        self.border_blink_alpha += self.border_blink_step
        if self.border_blink_alpha >= 255 or self.border_blink_alpha < 0:
            self.border_blink_step *= -1
            self.border_blink_alpha += self.border_blink_step
        green_alpha = pygame.Color(green_color.r, green_color.g, green_color.b, self.border_blink_alpha)

        pygame.draw.rect(border_alpha, green_alpha, surface.get_rect(), self.border_size)
        surface.blit(border_alpha, surface.get_rect().topleft)

    def _draw_name(self, combat_map_surface, center):
        name = self.font.render(self.name, True, self.font_color)
        name_rect = name.get_rect()
        name_rect.centerx = center[0]
        name_rect.centery = center[1] - name_rect.height / 2
        combat_map_surface.blit(name, name_rect)

    def _draw_hp(self, combat_map_surface, center):
        hp = self.font.render(f"HP: {self.hp}", True, self.font_color)
        hp_rect = hp.get_rect()
        hp_rect.centerx = center[0]
        hp_rect.centery = center[1] + hp_rect.height / 2
        combat_map_surface.blit(hp, hp_rect)

        if self.hp_lost:
            hp_flash = self.font.render(f"HP: {self.hp} - {self.hp_lost}", True, pygame.Color("red"))
            hp_flash.set_alpha(self.hp_flash_alpha)
            self.hp_flash_alpha -= self.hp_flash_alpha_step
            if self.hp_flash_alpha <= 0:
                self.hp_flash_alpha = 255
                self.hp_lost = 0
            hp_flash_rect = hp_flash.get_rect()
            hp_flash_rect.topleft = hp_rect.topleft
            combat_map_surface.blit(hp_flash, hp_rect)

    def draw(self):
        # char plate
        self.surface.fill(self.background_color)
        self.surface.blit(self.portrait, self.portrait_dest_rect)

        # borders
        pygame.draw.rect(
            self.surface,
            pygame.Color("grey"),
            self.surface.get_rect(),
            self.border_size,
        )
        if self.mouse_over:
            pygame.draw.rect(
                self.surface,
                self.mouse_over_color,
                self.surface.get_rect(),
                self.border_size,
            )
        if self.active:
            self.draw_active_border()

        self.draw_gui()
        if not self.alive:
            self.draw_dead_cover()

        return self.surface

    def draw_dead_cover(self):
        dead_cover = pygame.Surface.copy(self.surface)
        dead_cover.fill(pygame.Color("grey"))
        x_font = pygame.font.Font(None, int(self.surface.get_rect().height * 1.5))
        x_surface = x_font.render("X", True, pygame.Color("black"))
        x_rect = x_surface.get_rect()
        x_rect.center = dead_cover.get_rect().center
        dead_cover.blit(x_surface, x_rect)
        dead_cover.set_alpha(150)
        self.surface.blit(dead_cover, self.surface.get_rect().topleft)

    def draw_active_border(self):
        border_alpha = self.surface.copy()
        green_color = pygame.Color("green")

        self.border_blink_alpha += self.border_blink_step
        if self.border_blink_alpha >= 255 or self.border_blink_alpha < 0:
            self.border_blink_step *= -1
            self.border_blink_alpha += self.border_blink_step
        green_alpha = pygame.Color(green_color.r, green_color.g, green_color.b, self.border_blink_alpha)

        pygame.draw.rect(border_alpha, green_alpha, self.surface.get_rect(), self.border_size)
        self.surface.blit(border_alpha, self.surface.get_rect().topleft)

    def setup_portrait(self, portrait_file_path, portrait_size):
        raw_portrait = load_image(portrait_file_path, "characters").convert_alpha()
        portrait = pygame.transform.scale(raw_portrait, portrait_size)
        dest_rect = portrait.get_rect(center=self.surface.get_rect().center)
        return portrait, dest_rect


class Player(Character):
    active_weapon_slot_change_sound_filename = "klik0.mp3"
    action_points = 6
    player_keyboard_keys = (
        pygame.K_SPACE,
        pygame.K_DOWN,
        pygame.K_UP,
        pygame.K_LEFT,
        pygame.K_RIGHT,
        pygame.K_1,
        pygame.K_2,
    )

    def __init__(
        self,
        name,
        hp,
        dmg,
        screen_location: pygame.Rect,
        portrait_file_path,
        draw_settings: dict,
        sound_movement: pygame.mixer.Sound = None,
        sound_attack=None,
    ):
        Character.__init__(
            self,
            name=name,
            hp=hp,
            dmg=dmg,
            screen_location=screen_location,
            portrait_file_path=portrait_file_path,
            draw_settings=draw_settings,
            sound_movement=sound_movement if sound_movement else None,
            sound_attack=sound_attack if sound_attack else None,
        )
        self.active_weapon_slot = 0
        self.default_weapon = Weapon("fist", "fist.png", 0, 1)
        self.weapons = [self.default_weapon, Weapon("wooden club", "club_wood.png", 2, 3)]
        self.active_weapon_slot_change_sound = load_sound(Player.active_weapon_slot_change_sound_filename)
        self.key_to_weapon_slot = {pygame.K_1: 0, pygame.K_2: 1}
        self.key_to_move_direction = {
            pygame.K_DOWN: (0, 1),
            pygame.K_UP: (0, -1),
            pygame.K_LEFT: (-1, 0),
            pygame.K_RIGHT: (1, 0),
        }

    def handle_signals(self, signal_queue: GameSignalsQueue):
        received_signals = list()
        if self.active:
            received_signals.extend(
                signal_queue.get(drop_signal=True, signal_type=None, receiver=GameSignal.RECEIVER_ACTIVE_PLAYER)
            )
        for s in received_signals:
            if s.signal_type == GameSignal.TYPE_WEAPON_CHANGE:
                self.equip_weapon(s.data["slot_id"], s.data["weapon"])

    def equip_weapon(self, slot_id, weapon):
        assert slot_id in range(len(self.weapons))
        self.weapons[slot_id] = weapon if weapon else self.default_weapon

    def attack(self, target: "Character"):
        target.reduce_health(self.weapons[self.active_weapon_slot].roll_damage())
        self.sound_attack.play()
        self.action_points -= 3

    def synchronize_bar(self, player_bar: PlayersBar):
        for i, w in enumerate(self.weapons):
            player_bar.fill_weapon_slot(i, w)
        player_bar.activate_weapon_slot(self.active_weapon_slot)

    def try_move(self, key_pressed, characters, local_map, alive_characters_id):
        move_vector = pygame.Vector2(self.key_to_move_direction[key_pressed])
        new_tile_cords = self.position + move_vector
        enemy_at_target_position = [
            characters[char_id]
            for char_id in alive_characters_id
            if type(characters[char_id]) == Enemy and characters[char_id].position == new_tile_cords
        ]
        if enemy_at_target_position:
            self.attack(enemy_at_target_position[0])
        elif local_map.is_tile_available(new_tile_cords) and not len(
            [True for char_id in alive_characters_id if characters[char_id].position == new_tile_cords]
        ):
            self.move(move_vector)

    def handle_activation(self, characters, alive_characters_id, local_map, keyboard_keys_pressed):
        if self.action_ongoing:
            return
        if self.action_points <= 0:
            self.end_turn()
            return

        possible_moves_left = int(self.action_points / Character.movement_cost)
        local_map.highlighted_tiles = local_map.get_available_tiles_in_range(tuple(self.position), possible_moves_left)

        for k in keyboard_keys_pressed:
            if k == pygame.K_SPACE:
                self.end_turn()
            elif k in self.key_to_weapon_slot:
                self.activate_weapon_slot(k)
            elif k in self.key_to_move_direction:
                self.try_move(k, characters, local_map, alive_characters_id)

    def activate_weapon_slot(self, key):
        slot_id = self.key_to_weapon_slot[key]
        if slot_id != self.active_weapon_slot:
            self.active_weapon_slot_change_sound.play()
            self.active_weapon_slot = slot_id


class Enemy(Character):
    action_points = 3
    turn_clock = pygame.time.Clock()
    turn_time_taken = 0
    think_time = 500

    def init_for_turn(self, players_bar: PlayersBar = None):
        Character.init_for_turn(self)
        # todo: disable players bar

    def attack(self, target: "Character"):
        # reset clock on turn start
        if not self.turn_time_taken:
            self.turn_time_taken = 1
            self.turn_clock.tick()
            return

        self.turn_time_taken += self.turn_clock.tick()
        if self.turn_time_taken >= self.think_time:
            target.reduce_health(self.dmg)
            self.sound_attack.play()
            self.turn_finished = True
            self.active = False
            self.turn_time_taken = 0

    def draw_active_border(self):
        active_border = self.surface.copy()
        pygame.draw.rect(
            active_border,
            pygame.Color("green"),
            self.surface.get_rect(),
            self.border_size,
        )
        self.surface.blit(active_border, self.surface.get_rect().topleft)

    def handle_automated_activation(self, characters, alive_characters_id, local_map):
        if self.action_ongoing:
            return
        if self.action_points <= 0:
            self.end_turn()
            return

        alive_characters = [characters[char_id] for char_id in alive_characters_id]
        alive_players = [c for c in alive_characters if type(c) == Player]
        possible_targets = [c for c in alive_players if 1 == get_distance_taxi(c.position, self.position)]

        if possible_targets:
            self.attack(choice(possible_targets))
            return

        follow_player = choice((True, True, False))
        if follow_player:
            nearest_player_pos = min(
                [(get_distance_taxi(p.position, self.position), p.position) for p in alive_players], key=lambda x: x[0]
            )[1]
            possible_directions = get_directions_to_target(self.position, nearest_player_pos)
        else:
            possible_directions = list(map(pygame.Vector2, [(0, 0), (1, 0), (-1, 0), (0, 1), (0, -1)]))
        chosen_direction = choice(possible_directions)
        future_position = self.position + chosen_direction

        if future_position not in [c.position for c in alive_characters] and local_map.is_tile_available(
            future_position
        ):
            self.move(chosen_direction)
        else:
            self.end_turn()
