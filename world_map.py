import json
from random import choice
from typing import Tuple

import pygame as pg

from utils import load_image, MAPS_DIR

DEBUG = True


class MapInfo:
    def __init__(self, map_name):
        self.map_array = self.load_map_array(map_name)
        self.map_y_tiles, self.map_x_tiles = (len(self.map_array), len(self.map_array[0]))

    @staticmethod
    def load_map_array(map_name):
        with open(MAPS_DIR / map_name / "base.json", "r", encoding="utf-8") as f:
            return json.load(f)

    def get_available_tiles(self):
        return [
            pg.Vector2((x, y)) for x in range(self.map_x_tiles) for y in range(self.map_y_tiles) if self.map_array[y][x]
        ]

    def is_tile_available(self, tile_cords):
        if 0 <= tile_cords[0] < self.map_x_tiles and 0 <= tile_cords[1] < self.map_y_tiles:
            return bool(self.map_array[int(tile_cords[1])][int(tile_cords[0])])

    def get_nearest_available_tiles(self, origin: tuple):
        available_tiles = list()
        for vector in ((0, 1), (0, -1), (1, 0), (-1, 0)):
            tile_to_check = pg.Vector2(origin) + pg.Vector2(vector)
            if self.is_tile_available(tile_to_check):
                available_tiles.append(tile_to_check)
        return set(tuple(v) for v in available_tiles)

    def get_available_tiles_in_range(self, origin: tuple, range_tiles: int):
        if range_tiles <= 0:
            raise Exception(f"range_tiles is {range_tiles}")

        available_tiles = self.get_nearest_available_tiles(origin)
        if range_tiles == 1:
            return available_tiles
        else:
            next_available_tiles = set()
            for t in available_tiles:
                next_available_tiles = next_available_tiles.union(self.get_available_tiles_in_range(t, range_tiles - 1))
            return available_tiles.union(next_available_tiles)


class WorldMap(MapInfo):
    def __init__(self, map_array, map_view_size, background_color):
        MapInfo.__init__(self, map_array)
        # other
        self.players_tile_pos = [pg.Vector2(1, 0), pg.Vector2(0, 1), pg.Vector2(0, 0)]
        self.enemies_tile_pos = (4, 4)
        self.combat_start = False

        # tiles
        self.original_tile_image = load_image("grass0.png", "tiles").convert_alpha()
        self.tile_size = pg.Vector2(100, 100)
        self.tile_image = self.fit_surface_to_tile(self.original_tile_image)
        self.highlighted_tiles = []

        # map surface
        self.map_draw_size = None
        self.map_surface = None
        self.draw_starting_point = None
        self.update_map_surface_size(map_view_size)
        self.background_color = background_color

        # view port
        self.map_view_attach_point = self.draw_starting_point.copy()
        self.map_view = self.map_surface.subsurface(self.map_view_attach_point, map_view_size)
        self.map_render_bounds = None
        self.update_render_bounds()

    def zoom_map(self, zoom_step, multiplier):
        quarter_size_map_view = pg.Vector2(self.map_view.get_size()) / 2
        pre_zoom_mouse_map_cords = self.map_pixels_to_tile_cords(
            self.screen_pixels_to_map_pixels(quarter_size_map_view)
        )
        new_tile_size = self.tile_size + pg.Vector2(zoom_step, zoom_step) * multiplier
        if not (0 < new_tile_size[0] <= quarter_size_map_view[0] and 0 < new_tile_size[1] <= quarter_size_map_view[1]):
            return
        self.tile_size = new_tile_size
        self.tile_image = self.fit_surface_to_tile(self.original_tile_image)
        self.update_map_surface_size()
        new_map_view_attach_point = self.tile_cords_to_map_pixels(pre_zoom_mouse_map_cords) - quarter_size_map_view
        self.move_map_view(new_map_view_attach_point, reset=True)
        self.update_render_bounds()

    def update_map_surface_size(self, map_view_size=None):
        if not map_view_size:
            map_view_size = self.map_view.get_size()

        self.map_draw_size = pg.Vector2(self.map_x_tiles * self.tile_size[0], self.map_y_tiles * self.tile_size[1])
        empty_border_size = self.tile_size
        self.map_surface = pg.Surface(
            (
                max(self.map_draw_size[0], map_view_size[0]) + 2 * empty_border_size[0],
                max(self.map_draw_size[1], map_view_size[1]) + 2 * empty_border_size[1],
            )
        ).convert_alpha()
        self.draw_starting_point = empty_border_size

    def update_render_bounds(self):
        self.map_render_bounds = pg.Rect(
            self.map_view_attach_point - self.tile_size,
            pg.Vector2(self.map_view.get_size()) + self.tile_size,
        )

    def fit_surface_to_tile(self, portrait_surface):
        return pg.transform.scale(portrait_surface, self.tile_size)

    def draw(self, absolute_pos, source_surface):
        if self.map_render_bounds.collidepoint(absolute_pos):
            self.map_surface.blit(source_surface, absolute_pos)

    def move_map_view(self, input_vector: pg.Vector2, reset=False):
        """Moves map_view attach point relative to map_surface
        - reset - if True, set new attach point instead of moving current position
        """
        if reset:
            new_attach_point = input_vector
        else:
            new_attach_point = self.map_view_attach_point + input_vector
        new_map_view_rect = self.map_view.get_rect().move(new_attach_point)
        new_map_view_rect.clamp_ip(self.map_surface.get_rect())
        self.map_view_attach_point = pg.Vector2(new_map_view_rect.topleft)
        self.map_render_bounds.topleft = self.map_view_attach_point - self.tile_size
        self.map_view = self.map_surface.subsurface(new_map_view_rect)

    def tile_cords_to_map_pixels(self, tile_cords: pg.Vector2):
        relative_pos = pg.Vector2(tile_cords[0] * self.tile_size[0], tile_cords[1] * self.tile_size[1])
        absolute_pos = self.draw_starting_point + relative_pos
        return absolute_pos

    def map_pixels_to_tile_cords(self, absolute_pixels: pg.Vector2):
        relative_pixels = absolute_pixels - self.draw_starting_point
        return pg.Vector2(
            int(relative_pixels[0] / self.tile_size[0]),
            int(relative_pixels[1] / self.tile_size[1]),
        )

    def screen_pixels_to_map_pixels(self, screen_pixels: pg.Vector2):
        return self.map_view_attach_point + screen_pixels

    def highlight_tile(self, tile_cords: Tuple):
        alpha_cover = self.tile_image.copy()
        alpha_cover.fill(pg.Color(100, 100, 100, 100))
        highlighted_tile = self.tile_image.copy()
        highlighted_tile.blit(alpha_cover, (0, 0))
        self.highlighted_tiles.remove(tuple(tile_cords))
        return highlighted_tile

    def draw_map(self):
        pg.draw.rect(self.map_surface, self.background_color, self.map_render_bounds)
        for y_tile in range(self.map_y_tiles):
            for x_tile in range(self.map_x_tiles):
                if self.map_array[y_tile][x_tile]:
                    tile_cords = pg.Vector2(x_tile, y_tile)
                    tile_absolute_map_pixels = self.tile_cords_to_map_pixels(tile_cords)
                    if tuple(tile_cords) in self.highlighted_tiles:
                        self.draw(
                            tile_absolute_map_pixels,
                            self.highlight_tile(tuple(tile_cords)),
                        )
                    else:
                        self.draw(tile_absolute_map_pixels, self.tile_image)

                    # if tuple(mouse_tile_pos) == (x_tile, y_tile):
                    #     pygame.draw.rect(
                    #         surface=self.map_surface, color=DRAW_SETTINGS["mouse_over_color"],
                    #         rect=pygame.Rect(tile_absolute_map_pixels, self.tile_size),
                    #         width=5)


def get_scroll_settings(screen_size):
    scroll_trigger_border_size = 20
    scroll_trigger_border = {
        "top": pg.Rect(0, 0, screen_size[0], scroll_trigger_border_size),
        "down": pg.Rect(
            0,
            screen_size[1] - scroll_trigger_border_size,
            screen_size[0],
            scroll_trigger_border_size,
        ),
        "left": pg.Rect(0, 0, scroll_trigger_border_size, screen_size[1]),
        "right": pg.Rect(
            screen_size[0] - scroll_trigger_border_size,
            0,
            scroll_trigger_border_size,
            screen_size[1],
        ),
    }
    scroll_border_color = {
        "top": "red",
        "down": "green",
        "left": "blue",
        "right": "yellow",
    }
    scroll_speed = 20
    scroll_vector = {
        "top": pg.Vector2(0, scroll_speed),
        "down": pg.Vector2(0, -scroll_speed),
        "left": pg.Vector2(scroll_speed, 0),
        "right": pg.Vector2(-scroll_speed, 0),
    }
    return scroll_trigger_border, scroll_border_color, scroll_speed, scroll_vector


def generate_team_token(tile_size, players):
    token_surface = pg.Surface(tile_size).convert_alpha()
    for player in players:
        scale_transform = array(token_surface.get_size()) / 2
        mini_portrait = pg.Surface(scale_transform).convert_alpha()
        pg.transform.scale(player.portrait, scale_transform, mini_portrait)
        token_surface.blit(
            source=mini_portrait,
            dest=(
                choice(range(token_surface.get_width() - mini_portrait.get_width())),
                choice(range(token_surface.get_height() - mini_portrait.get_height())),
            ),
        )
        token_surface.set_colorkey(pg.Color("black"))
    return token_surface


def world_map(screen, background_color, font, players, enemies):
    (
        scroll_trigger_border,
        scroll_border_color,
        scroll_speed,
        scroll_vector,
    ) = get_scroll_settings(screen.get_size())

    map_array = array(
        [
            [1, 1, 0, 0, 1],
            [0, 1, 1, 1, 0],
            [0, 0, 0, 1, 1],
            [1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1],
        ]
    )
    map_obj = WorldMap(map_array, screen.get_size(), background_color)
    map_scroll_max_rect = pg.Rect(
        0,
        0,
        map_obj.map_draw_size[0] - map_obj.map_view.get_width(),
        map_obj.map_draw_size[1] - map_obj.map_view.get_height(),
    )

    team_token = generate_team_token(map_obj.tile_size, players)
    enemies_token = generate_team_token(map_obj.tile_size, enemies)

    map_scroll_acc = array((0, 0))
    clock = pg.time.Clock()
    while True:
        for e in pg.event.get():
            if e.type == pg.QUIT:
                exit(0)
            if e.type == pg.KEYDOWN:
                if e.key == pg.K_DOWN:
                    map_obj.move_players((0, 1))
                elif e.key == pg.K_UP:
                    map_obj.move_players((0, -1))
                elif e.key == pg.K_LEFT:
                    map_obj.move_players((-1, 0))
                elif e.key == pg.K_RIGHT:
                    map_obj.move_players((1, 0))

        if map_obj.combat_start:
            return

        map_mouse_pos = map_scroll_acc + array(pg.mouse.get_pos())
        map_mouse_pos_tiles = [int(cord / tile_dim) for cord, tile_dim in zip(map_mouse_pos, map_obj.tile_size)]

        # draw_border_over_mouse_tile()
        for direction, rect in scroll_trigger_border.items():
            if pg.mouse.get_focused() and rect.collidepoint(pg.mouse.get_pos()):
                if not map_scroll_max_rect.collidepoint(map_scroll_acc):
                    map_scroll_acc += scroll_vector[direction]
                    continue
                map_scroll_acc -= scroll_vector[direction]
                map_obj.move_map_view(scroll_vector[direction])
        map_obj.draw_map(map_mouse_pos_tiles, team_token, enemies_token)

        screen.blit(map_obj.map_view, map_obj.map_view.get_rect())

        if DEBUG:
            for side, rect in scroll_trigger_border.items():
                pg.draw.rect(screen, pg.Color(scroll_border_color[side]), rect)

            map_scroll_rendered = font.render(f"map scroll: {map_scroll_acc}", True, pg.Color("black"))
            screen.blit(map_scroll_rendered, (0, 0))

            map_cords_rendered = font.render(f"mouse over tile: {map_mouse_pos_tiles}", True, pg.Color("black"))
            screen.blit(map_cords_rendered, (0, 36))

        pg.display.flip()
        clock.tick(60)


def main():
    pg.init()
    window_size = (600, 600)
    screen = pg.display.set_mode(window_size)

    font = pg.font.Font(None, 36)
    background_color = pg.Color("grey")
    screen.fill(background_color)

    players = list()
    world_map(screen, background_color, font, players)


if __name__ == "__main__":
    main()
