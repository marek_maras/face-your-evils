from typing import List, Optional, Tuple

import pygame as pg

from utils import load_image, load_sound, GameSignal, Weapon


class PlayersBar:
    bar_height = 75
    border = 5
    weapon_active_border_thickness = 2
    assert weapon_active_border_thickness < border
    weapon_active_border_color = pg.Color("black")
    weapon_inactive_border_color = pg.Color("white")
    weapon_slots = [0, 1]
    action_done_sound_filename = "klik0.mp3"
    empty_slot_color = pg.Color("grey")

    def __init__(self, screen_size):
        self.size = (screen_size[0], self.bar_height)
        self.surface = self.prepare_bar_surface()
        self.screen_rect = pg.Rect((0, screen_size[1] - self.bar_height), self.size)
        self.weapon_slot_bar_rects, self.weapon_slot_screen_rects = self.prepare_weapon_slot_rects()
        self.weapon_slot_surfaces: List[Optional[pg.Surface]] = [None] * len(self.weapon_slots)
        self.active_weapon_slot = None
        self.weapons: List[Optional[Weapon]] = [None, None]
        self.fill_weapon_slot(0, self.weapons[0])
        self.fill_weapon_slot(1, self.weapons[1])
        self.action_done_sound = load_sound(PlayersBar.action_done_sound_filename)

    def prepare_bar_surface(self):
        bar = pg.Surface(self.size).convert_alpha()
        bar.fill(pg.Color("grey"))
        return bar

    def prepare_weapon_slot_rects(self) -> Tuple[List[pg.Rect], List[pg.Rect]]:
        weapon_slot_size = (50, self.bar_height - 2 * self.border)
        weapon_slot_rects = list()
        first_slot_topleft = (self.border, self.border)
        weapon_slot_rects.append(pg.Rect(first_slot_topleft, weapon_slot_size))
        second_slot_topleft = (
            first_slot_topleft[0] + weapon_slot_size[0] + self.border,
            first_slot_topleft[1],
        )
        weapon_slot_rects.append(pg.Rect(second_slot_topleft, weapon_slot_size))
        assert len(weapon_slot_rects) == len(self.weapon_slots)
        weapon_slot_screen_rects = [r.move(self.screen_rect.topleft) for r in weapon_slot_rects]
        return weapon_slot_rects, weapon_slot_screen_rects

    def fill_weapon_slot(self, slot_id: int, weapon: Optional[Weapon]):
        if slot_id not in self.weapon_slots:
            raise Exception(f"wrong slot: {slot_id}")
        if self.weapons[slot_id] == weapon:
            return

        # clear background form previous weapon
        self.weapons[slot_id] = weapon
        self.weapon_slot_surfaces[slot_id] = pg.Surface(self.weapon_slot_bar_rects[slot_id].size).convert_alpha()
        self.weapon_slot_surfaces[slot_id].fill(self.empty_slot_color)

        if weapon is not None:
            self.surface.blit(self.weapon_slot_surfaces[slot_id], self.weapon_slot_bar_rects[slot_id])
            self.weapon_slot_surfaces[slot_id] = pg.transform.scale(
                weapon.image, self.weapon_slot_bar_rects[slot_id].size
            )

        self.surface.blit(self.weapon_slot_surfaces[slot_id], self.weapon_slot_bar_rects[slot_id])
        self.draw_weapon_border(slot_id, active=self.active_weapon_slot == slot_id)

    def draw_weapon_border(self, slot_id, active: bool):
        if active:
            color = self.weapon_active_border_color
        else:
            color = self.weapon_inactive_border_color

        pg.draw.rect(
            self.surface,
            color,
            self.weapon_slot_bar_rects[slot_id],
            self.weapon_active_border_thickness,
        )

    def activate_weapon_slot(self, slot_id):
        if slot_id == self.active_weapon_slot:
            return
        if slot_id not in self.weapon_slots:
            raise Exception(f"wrong slot: {slot_id}")

        if self.active_weapon_slot is not None:
            self.draw_weapon_border(self.active_weapon_slot, False)

        self.active_weapon_slot = slot_id
        self.draw_weapon_border(self.active_weapon_slot, True)

    def mouse_drop(self, g_signal: GameSignal, mouse_event: pg.event):
        mouse_over_weapon_slot = [
            slot_id for slot_id, rect in enumerate(self.weapon_slot_screen_rects) if rect.collidepoint(mouse_event.pos)
        ]
        if mouse_over_weapon_slot and mouse_event.button == pg.BUTTON_LEFT:
            slot_id = mouse_over_weapon_slot[0]
            return [
                GameSignal(
                    signal_type=GameSignal.TYPE_WEAPON_CHANGE,
                    sender=self.__class__.__name__,
                    data=dict(slot_id=slot_id, weapon=g_signal.data["weapon"]),
                    receiver=GameSignal.RECEIVER_ACTIVE_PLAYER,
                )
            ]

    def mouse_down(self, mouse_event: pg.event):
        mouse_over_weapon_slot = [
            slot_id for slot_id, rect in enumerate(self.weapon_slot_screen_rects) if rect.collidepoint(mouse_event.pos)
        ]
        if mouse_over_weapon_slot and mouse_event.button == pg.BUTTON_LEFT:
            slot_id = mouse_over_weapon_slot[0]
            weapon = self.weapons[slot_id]
            # 1. bar weapon slot set to None
            self.fill_weapon_slot(slot_id, None)
            weapon.sound_drag.play()
            return [
                # 2. request player default weapon as replacement
                GameSignal(
                    signal_type=GameSignal.TYPE_WEAPON_CHANGE,
                    sender=self.__class__.__name__,
                    data=dict(slot_id=slot_id, weapon=None),
                    receiver=GameSignal.RECEIVER_ACTIVE_PLAYER,
                ),
                # 3. indicate item is being dragged on screen
                GameSignal(
                    signal_type=GameSignal.TYPE_ITEM_DRAG,
                    sender=self.__class__.__name__,
                    data=dict(weapon=weapon),
                    receiver=GameSignal.RECEIVER_CURSOR_RENDER,
                ),
            ]
