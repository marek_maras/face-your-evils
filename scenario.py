from random import choice
from typing import List

from characters import Player, Enemy, Character
from combat import handle_turn_order, get_winners
from players_bar import PlayersBar
from utils import DRAW_SETTINGS, show_fps, load_image, load_sound, GameSignalsQueue, GameSignal
from world_map import get_scroll_settings, WorldMap
import pygame as pg

DEBUG = True


class ScenarioMap(WorldMap):
    pass


def scenario(screen, players, enemies):
    font = pg.font.Font(None, DRAW_SETTINGS["font_size"])
    (
        scroll_trigger_border,
        scroll_border_color,
        scroll_speed,
        scroll_vector,
    ) = get_scroll_settings(screen.get_size())

    bottom_bar = PlayersBar(screen.get_size())

    local_map = ScenarioMap(
        "arena",
        (screen.get_size()[0], screen.get_size()[1] - bottom_bar.bar_height),
        DRAW_SETTINGS["background_color"],
    )

    # prepare map entities
    for i, player in enumerate(players):
        player.init_for_local_map(local_map, local_map.players_tile_pos[i])

    characters: List[Character] = players
    available_tiles = local_map.get_available_tiles()
    for enemy in enemies:
        is_tile_available = False
        while not is_tile_available:
            chosen_start_tile = choice(available_tiles)
            if not [True for char in characters if chosen_start_tile == char.position]:
                is_tile_available = True
        enemy.init_for_local_map(local_map, chosen_start_tile)
        characters.append(enemy)

    # turn order control
    turn_id = 0
    alive_characters_id = [idx for idx, _ in enumerate(characters)]
    active_character: Character = characters[alive_characters_id[turn_id]]
    active_character.turn_finished = False

    winners = None
    clock = pg.time.Clock()
    map_scroll_acc = pg.Vector2((0, 0))
    zoom_step = 100
    signal_queue = GameSignalsQueue()
    while not winners:
        active_character, turn_id = handle_turn_order(characters, active_character, alive_characters_id, turn_id)
        if not active_character.active:
            active_character.init_for_turn()

        if type(active_character) == Enemy:
            active_character.handle_automated_activation(characters, alive_characters_id, local_map)

        player_keys_pressed = list()
        for e in pg.event.get():
            if e.type == pg.QUIT:
                exit(0)
            if e.type == pg.MOUSEWHEEL:
                local_map.zoom_map(zoom_step, e.y)
                player.map_portrait = local_map.fit_surface_to_tile(player.portrait)
                enemy.map_portrait = local_map.fit_surface_to_tile(enemy.portrait)
            if e.type == pg.MOUSEBUTTONDOWN:
                if bottom_bar.screen_rect.collidepoint(e.pos):
                    signal_queue.add(bottom_bar.mouse_down(e))
            if e.type == pg.MOUSEBUTTONUP:
                g_signals = signal_queue.get(drop_signal=True, signal_type=GameSignal.TYPE_ITEM_DRAG)
                if g_signals:
                    g_signals[0].data["weapon"].sound_drop.play()
                    if bottom_bar.screen_rect.collidepoint(e.pos):
                        signal_queue.add(bottom_bar.mouse_drop(g_signals[0], e))
            if e.type == pg.KEYDOWN:
                if e.key in Player.player_keyboard_keys:
                    player_keys_pressed.append(e.key)

        if type(active_character) == Player:
            active_character.handle_activation(characters, alive_characters_id, local_map, player_keys_pressed)
            active_character.synchronize_bar(bottom_bar)
            active_character.handle_signals(signal_queue)

        map_mouse_pos = local_map.screen_pixels_to_map_pixels(pg.Vector2(pg.mouse.get_pos()))

        for direction, rect in scroll_trigger_border.items():
            if pg.mouse.get_focused() and rect.collidepoint(pg.mouse.get_pos()):
                local_map.move_map_view(-1 * scroll_vector[direction])
                map_scroll_acc = local_map.draw_starting_point - local_map.map_view_attach_point

        local_map.draw_map()
        for id in alive_characters_id:
            characters[id].update(local_map)
            characters[id].draw_on_combat_map(local_map)
        screen.blit(local_map.map_view, local_map.map_view.get_rect())
        screen.blit(bottom_bar.surface, bottom_bar.screen_rect)

        if DEBUG:
            for side, rect in scroll_trigger_border.items():
                pg.draw.rect(screen, pg.Color(scroll_border_color[side]), rect)

            map_scroll_rendered = font.render(f"map scroll: {map_scroll_acc}", True, pg.Color("black"))
            screen.blit(map_scroll_rendered, (0, 0))

            map_cords_rendered = font.render(
                f"mouse over tile: {local_map.map_pixels_to_tile_cords(map_mouse_pos)}",
                True,
                pg.Color("black"),
            )
            screen.blit(map_cords_rendered, (0, 36))

            show_fps(screen, clock, DRAW_SETTINGS["font_size"])

        dragged_item_signals = signal_queue.get(drop_signal=False, signal_type=GameSignal.TYPE_ITEM_DRAG)
        if dragged_item_signals:
            screen.blit(dragged_item_signals[0].data["weapon"].icon, pg.mouse.get_pos())

        tmp_list = alive_characters_id
        for char_id in tmp_list:
            if not characters[char_id].alive:
                alive_characters_id.remove(char_id)
        winners = get_winners(characters, alive_characters_id)

        pg.display.flip()
        clock.tick(60)

    print("KONIEC")
    exit(0)
