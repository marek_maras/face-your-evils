#!/usr/bin/python
import pygame
from pygame.locals import *

from characters import Player, Enemy
from combat import prepare_combat
from scenario import scenario
from utils import DRAW_SETTINGS, get_character_board_height, load_sound
from world_map import world_map


def main():
    # Initialise screen
    pygame.init()
    screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
    screen_size = screen.get_size()
    pygame.display.set_caption("Basic Pygame program")

    # Fill next_screen
    background = pygame.Surface(screen.get_size())
    background = background.convert_alpha()
    background_color = (250, 250, 200)
    background.fill(background_color)

    # fill some global settings
    DRAW_SETTINGS["portrait_size"] = (screen_size[0] / 10, screen_size[1] / 2 * 4 / 5)

    # Display some text
    font = pygame.font.Font(None, DRAW_SETTINGS["font_size"])
    text = font.render("Hello There", True, DRAW_SETTINGS["font_color"])
    textpos = text.get_rect()
    textpos.center = background.get_rect().center
    background.blit(text, textpos)

    # Blit everything to the screen
    screen.blit(background, (0, 0))
    pygame.display.flip()

    players = create_players(screen_size[0], screen_size[1])
    enemies = create_enemies(screen_size[0], screen_size[1])

    # Event loop
    while 1:
        for event in pygame.event.get():
            if event.type == QUIT:
                return
            if event.type == MOUSEBUTTONUP:
                scenario(screen, players, enemies)
                world_map(screen, background_color, font, players, enemies)
                prepare_combat(screen, background, players, enemies)

        screen.blit(background, (0, 0))
        pygame.display.flip()


def create_players(screen_width, screen_height):
    num_of_players = 2
    players = list()
    names = ("Kruszonka", "Dzik", "Cinu")
    portraits = ("girl0.png", "boarman0.png", "lizard0.png")
    walk_sounds = ("tupanie0.mp3", "dziczenie0.mp3")
    attack_sounds = ("chtrz0.mp3", "dziczy_atak0.mp3")
    for i in range(num_of_players):
        players.append(
            Player(
                names[i],
                10,
                3,
                pygame.Rect(
                    screen_width / num_of_players * i,
                    0,
                    screen_width / num_of_players,
                    get_character_board_height(screen_height),
                ),
                portraits[i],
                DRAW_SETTINGS,
                load_sound(walk_sounds[i]),
                load_sound(attack_sounds[i]),
            )
        )
    return players


def create_enemies(screen_width, screen_height):
    num_of_enemies = 12
    enemies = list()
    stats = ((10, 2), (2, 1), (2, 1), (5, 1)) * 3
    for i in range(num_of_enemies):
        gui_top_left_x = screen_width / num_of_enemies * i
        gui_length = screen_width / num_of_enemies
        name = f"zombie{i}"
        hp = stats[i][0]
        dmg = stats[i][1]
        enemies.append(
            Enemy(
                name,
                hp,
                dmg,
                pygame.Rect(
                    gui_top_left_x,
                    screen_height / 2,
                    gui_length,
                    get_character_board_height(screen_height),
                ),
                "zombie0.png",
                DRAW_SETTINGS,
                load_sound("powloczenie0.mp3"),
                load_sound("zombie_attack0.mp3"),
            )
        )
    return enemies


if __name__ == "__main__":
    main()
