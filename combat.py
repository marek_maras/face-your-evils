from random import choice
from typing import List

import pygame

from characters import Enemy, Player, Character
from utils import (
    blit_to_surface_center,
    show_fps,
    DRAW_SETTINGS,
    create_text_window,
)


def get_winners(characters, alive_characters_id):
    if not any(type(characters[char_id]) == Player for char_id in alive_characters_id):
        return "enemies"
    elif not any(type(characters[char_id]) == Enemy for char_id in alive_characters_id):
        return "players"


def handle_turn_order(characters, active_character, alive_characters_id, turn_id):
    if active_character.turn_finished:
        turn_id += 1
        if turn_id >= len(alive_characters_id):
            turn_id = 0
            for char_id in alive_characters_id:
                characters[char_id].turn_finished = False
    active_character = characters[alive_characters_id[turn_id]]
    return active_character, turn_id


def handle_events(active_character, characters):
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit(0)
        if event.type == pygame.MOUSEBUTTONUP:
            if type(active_character) == Player:
                target = [
                    char
                    for char in characters
                    if type(char) == Enemy and char.screen_rect.collidepoint(pygame.mouse.get_pos())
                ]
                if target:
                    active_character.attack(target[0])
        if event.type == pygame.MOUSEMOTION:
            for character in characters:
                character.mouse_over = character.screen_rect.collidepoint(pygame.mouse.get_pos())


def combat(screen, next_screen, characters, round_id):
    clock = pygame.time.Clock()

    round_window = create_text_window(f"ROUND {round_id}")
    turn_id = 0
    alive_characters_id = [idx for idx, _ in enumerate(characters)]
    active_character = characters[alive_characters_id[turn_id]]
    active_character.turn_finished = False
    winners = None
    while not winners:
        active_character, turn_id = handle_turn_order(characters, active_character, alive_characters_id, turn_id)

        if type(active_character) == Enemy:
            possible_targets_id = [char_id for char_id in alive_characters_id if type(characters[char_id]) == Player]
            active_character.attack(characters[choice(possible_targets_id)])

        handle_events(active_character, characters)

        next_screen.fill(DRAW_SETTINGS["background_color"])
        for character in characters:
            next_screen.blit(character.draw_map(), character.screen_rect.topleft)

        tmp_list = alive_characters_id
        for char_id in tmp_list:
            if not characters[char_id].alive:
                alive_characters_id.remove(char_id)
        winners = get_winners(characters, alive_characters_id)

        show_fps(next_screen, clock, DRAW_SETTINGS["font_size"])
        blit_to_surface_center(next_screen, round_window)
        screen.blit(next_screen, (0, 0))
        pygame.display.flip()
        pygame.display.update()
        clock.tick(60)

    combat_end(winners, screen)
    return winners


def prepare_combat(screen, next_screen, players, enemies):
    round_id = 1
    characters: List[Character] = players + enemies
    combat(screen, next_screen, characters, round_id)


def combat_end(winners, screen):
    text = dict(players="congrats, you won!", enemies="you have been eaten!")[winners]
    window = create_text_window(text)

    while 1:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                exit(0)
            if event.type == pygame.MOUSEBUTTONUP:
                return

        blit_to_surface_center(screen, window)
        pygame.display.flip()
