from pathlib import Path
from random import choice
from typing import Optional, Dict, Tuple, List

import pygame

DRAW_SETTINGS = {
    "font_size": 36,
    "font_color": pygame.Color("black"),
    "background_color": pygame.Color(250, 250, 200),
    "mouse_over_color": pygame.Color("grey34"),
    "window": {
        "border_size": 10,
        "border_color": pygame.Color("grey"),
    },
    "character": {
        "border_size": 10,
        "portrait_size": None,
    },
}
ROOT_DIR = Path("/home/sztylet/workspace/pygame_projects/face-your-evils")
MEDIA_DIR = ROOT_DIR / "media"
MAPS_DIR = ROOT_DIR / "maps"


def load_sound(file):
    """because pygame can be be compiled without mixer."""
    if not pygame.mixer:
        return None
    file = MEDIA_DIR / "sounds" / file
    try:
        sound = pygame.mixer.Sound(file.__str__())
        return sound
    except pygame.error:
        print("Warning, unable to load, %s" % file)
    return None


def load_image(filename: str, subfolder: str = "") -> pygame.Surface:
    if subfolder and subfolder not in ["weapons", "characters", "tiles"]:
        raise Exception(f"incorrect image subfolder: {subfolder}")
    return pygame.image.load(MEDIA_DIR / "images" / subfolder / filename)


def show_fps(surface: pygame.Surface, inner_clock: pygame.time.Clock, font_size: int) -> None:
    """
    Display at the top left corner of the screen the current frame rate.
    Keyword arguments:
    screen -- the surface on which the framerate should be drawn
    inner_clock -- the pygame clock running and containing the current frame rate
    """
    font = pygame.font.Font(None, font_size)
    fps_text: pygame.Surface = font.render("FPS: " + str(round(inner_clock.get_fps())), True, (0, 0, 0))
    surface.blit(fps_text, (0, 50))


def blit_to_surface_center(background, foreground):
    foreground_rect = foreground.get_rect()
    foreground_rect.center = background.get_rect().center
    background.blit(foreground, foreground_rect)


def create_text_window(text):
    font = pygame.font.Font(None, DRAW_SETTINGS["font_size"])
    rendered_text = font.render(text, True, DRAW_SETTINGS["font_color"])
    text_background = rendered_text.copy()
    text_background.fill(DRAW_SETTINGS["background_color"])
    text_background.blit(rendered_text, text_background.get_rect())
    window = pygame.Surface([i + DRAW_SETTINGS["window"]["border_size"] * 2 for i in text_background.get_size()])
    window.fill(DRAW_SETTINGS["window"]["border_color"])
    blit_to_surface_center(window, text_background)
    return window


def get_character_board_height(screen_height):
    return screen_height / 2


class GameSignal:
    """implements signals which can represent mouse click, state change or other mouse_event in any object which should be
    responded to elsewhere"""

    TYPE_ITEM_DRAG = 1
    TYPE_ITEM_DROP = 2
    TYPE_WEAPON_CHANGE = 3
    signal_types = (TYPE_ITEM_DRAG, TYPE_ITEM_DROP, TYPE_WEAPON_CHANGE)
    RECEIVER_ACTIVE_PLAYER = 1
    RECEIVER_CURSOR_RENDER = 2
    allowed_receivers = (RECEIVER_ACTIVE_PLAYER, RECEIVER_CURSOR_RENDER)

    def __init__(self, signal_type: int, sender, receiver, data: Dict):
        assert signal_type in GameSignal.signal_types
        assert receiver in GameSignal.allowed_receivers
        self.signal_type = signal_type
        self.sender = sender
        self.receiver = receiver
        self.data = data


class GameSignalsQueue:
    """class for holding events which can be created and retrieved by various objects in the game"""

    def __init__(self):
        self.queue = list()

    def add(self, g_signals: List[Optional[GameSignal]]):
        if not g_signals:
            return
        self.queue.extend(g_signals)

    def get(self, drop_signal: bool, signal_type: Optional[int], receiver: Optional = None) -> List[GameSignal]:
        if signal_type is None and receiver is None:
            raise Exception("event get called without filters!")

        matched_signals = list()
        for gs in self.queue:
            if signal_type is None or gs.signal_type == signal_type:
                if receiver is None or gs.receiver == receiver:
                    matched_signals.append(gs)

        if drop_signal:
            for gs in matched_signals:
                self.queue.remove(gs)

        return matched_signals


class Weapon:
    ICON_SIZE = (100, 100)
    media_loaded = False
    sound_drag = "cling0.mp3"
    sound_drop = "brzdek0.mp3"

    def __init__(self, name, image_file: str, damage_min, damage_max):
        if not self.media_loaded:
            self.load_media()
        self.name = name
        self.image = load_image(image_file, "weapons")
        self.damage_min = damage_min
        self.damage_max = damage_max
        self.icon = pygame.transform.scale(self.image, Weapon.ICON_SIZE)

    @classmethod
    def load_media(cls):
        cls.sound_drop = load_sound(cls.sound_drop)
        cls.sound_drag = load_sound(cls.sound_drag)
        cls.media_loaded = True

    def roll_damage(self):
        return choice(range(self.damage_min, self.damage_max + 1))


def get_distance_taxi(p1, p2):
    """calc distance between 2D points in taxi metric"""
    return abs(p1[0] - p2[0]) + abs(p1[1] - p2[1])


def get_directions_to_target(source, target) -> List:
    """get possible vectors which moves source closer to target"""
    x_distance = target[0] - source[0]
    y_distance = target[1] - source[1]
    directions = []
    if x_distance:
        directions.append(pygame.Vector2(x_distance, 0).normalize())
    if y_distance:
        directions.append(pygame.Vector2(0, y_distance).normalize())
    return directions
